\documentclass[a4paper,12pt]{article}

\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{microtype}
\usepackage{geometry}
\geometry{margin=24mm}

\usepackage{lettrine}
\usepackage{csquotes}

%~ \setmainfont[
    %~ Path           = libre-baskerville/,
    %~ Extension      = .otf,
    %~ Ligatures      = TeX,
    %~ BoldFont       = LibreBaskerville-Bold,
    %~ ItalicFont     = LibreBaskerville-Italic
%~ ]{LibreBaskerville-Regular}

%~ https://www.fontsquirrel.com/fonts/libre-baskerville

%~ \setmainfont{LibreBaskerville}

\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\setlength{\columnsep}{6mm} 

%~ \usepackage{lastpage}
%~ \usepackage{fancyhdr}
%~ \pagestyle{fancy}
%~ \fancyhf{}
%~ \fancyfoot[C]{\thepage \hspace{1pt} / \pageref{LastPage}}
%~ \renewcommand{\headrulewidth}[0]{0pt}

\begin{document}

%~ \thispagestyle{empty}

\begingroup  
  \centering
  \LARGE Les années d’apprentissage\\[1em]
  \LARGE de Charlotte Corday\\[1em]
  \normalsize par\\[1em]
  \large G. L\textsc{enôtre}\\[2.5em]\par
\endgroup

%~ \section*{\centering Adam, Ève, la famille.}

\lettrine{N}{’est-ce} pas qu’on a quelque peine à se représenter Charlotte Corday petite fille, bien sage sur sa grande chaise et berçant sa poupée ? Son acte de tranquille héroïsme absorbe si bien l’attention que toute sa vie semble tenir dans ces dix jours que l’on compte depuis son départ de Caen jusqu’à sa mort sur l’échafaud. Pourtant, elle avait alors vingt-cinq ans ; mais, de ses vingt-cinq ans, on ne sait rien, ou presque rien. On formerait une bibliothèque en réunissant les ouvrages qui traitent de son stoïque forfait ; et tout ce que les chroniqueurs et les historiens, en s’évertuant, nous ont appris de ses paisibles années, occupe à peine quelques pages.

De patientes enquêtes en Normandie ont permis à l’un de ces historiens, M. Eugène Defrance, de grouper ce qu’ont recueilli à ce sujet les collections particulières ou les archives locales, et de donner un récit de l’enfance et de la jeunesse de Charlotte Corday d’autant plus précieux qu’il ne rapporte, comme on doit s’y attendre, aucun évènement notable. C’est la vie d’une fillette bien portante, à demi-campagnarde, élevée sans gâterie, à la dure, honnête, droite, franche, réservée, sans passion, sans coquetterie et sans aventures.

Sur le territoire de la commune des Champeaux, dans l’arrondissement d’Argentan, se trouve la ferme du Ronceray. C’est aujourd’hui comme il y a cent cinquante ans, une simple chaumière normande, construite en pisé avec pans de bois apparents, ne comportant qu’un rez-de-chaussée surmonté d’un haut toit que percent trois grandes lucarnes et une lourde cheminée de maçonnerie. Charlotte naquit là le 27 juillet 1768 ; mais elle y resta peu de temps et quelques mois plus tard ses parents allaient se fixer au manoir de Corday, autre maison paysanne, située sur le territoire de la paroisse du Mesnil-Imbert, et qui ne différait du Ronceray que par une cour plantée d’arbres agrémentée d’un puits et entourée d’un mur garni de lierre. Au niveau de la cour, le manoir comprend une salle à manger, une laverie, une seconde salle, un office et une cuisine où subsiste encore l’ancienne cheminée supportée par deux colonnes de pierre. Le premier étage se compose de deux grandes chambres dont l’une fut celle de M\up{me} de Corday, d’un oratoire, et d’une troisième pièce éclairée d’une seule fenêtre qui ouvre sur le splendide panorama de la vallée d’Auge : c’était la chambre de Charlotte. Il y quelque cinquante ans, M. Vatel, l’historien passionné de l’héroïne, y trouva encore le lit étroit où elle couchait, ainsi que ses rideaux et son couvre-pieds de soie bleue ; il se rendit acquéreur de ces reliques qu’il légua plus tard au musée de Versailles.

Quoique nobles, M. et M\up{me} de Corday n’étaient pas riches : ils avaient deux fils et trois filles. Le père, homme doux et grave, ne cachait pas la modicité de ses ressources ; ses enfants connaissaient les sacrifices qu’il s’imposait et chacun d’eux s’ingéniait à l’économie. À moins d’un quart de lieu du manoir se trouvait le château de Glatigny qu’habitaient les aînés de la famille, les Corday-Glatigny. On fréquentait journellement d’une maison à l’autre ; on s’appelait à son de trompe et Charlotte avait sa chambre à Glatigny, où toute enfant elle aimait à séjourner. Mais la gêne croissante de ses parents l’obligea à quitter le manoir ; elle fut envoyée à Vicques, chez l’abbé de Corday, son oncle ; elle y vécut plusieurs années. C’est là qu’elle apprit à lire dans un exemplaire des œuvres de Pierre Corneille, son grand ancêtre, exemplaire que le bon curé conservait précieusement comme un souvenir de famille et qu’il se plut à utiliser pour l’instruction de sa nièce. L’abbé de Corday était un excellent prêtre ; il devait survivre longtemps à son élève puisqu’il mourut en 1825, curé de Coulibœuf, où il a laissé la mémoire d’une grande piété et d’une charité exemplaire.

Charlotte rentra chez ses parents – elle avait treize ans – pour assister aux derniers jours de sa mère qui s’éteignit, épuisée par la naissance d’un sixième enfant, en 1782 ; M. de Corday avait à cette époque quitté le manoir et s’était logé à Caen, dans une petite maison de la butte Saint-Gilles ; sa fille aînée décéda peu de temps après la mère de famille, et il dut se séparer des deux autres fillettes, qui obtinrent la faveur d’entrer au couvent de la Trinité de l’Abbaye-aux-Darnes, dont M\up{me} de Belzunce était la prieure. Charlotte s’y montra laborieuse, bonne, prévenante envers tous et d’une grande fermeté de caractère ; elle se jeta d’abord avec enthousiasme dans les pratiques d’une religion exaltée, puis bientôt assagie, elle manifesta un goût singulier pour les lectures les plus graves : Corneille, dont elle ne se lassait point, Plutarque, Rousseau, Voltaire et Raynal étaient ses auteurs favoris ; elle lisait aussi la vie des saints et d’autres ouvrages pieux ; le musée Carnavalet conserve un exemplaire du \textit{Typus mundi,} édition de 1627, relié de vélin blanc avec fleurons d’or aux angles, qui porte au verso du faux titre ces mots écrits de la main de Charlotte :

\begin{displayquote}
Acheté 4 livres. Corday d’Armont

Sainte-Trinité de Caen, 20 décembre 1790.
\end{displayquote}

Peu de jours après cette dépense de quatre livres, lourde pour sa maigre bourse, elle quitta le couvent et vint retrouver son père au manoir du Mesnil-Imbert. Là elle partageait sa vie entre sa petite chambre et le château voisin de Glatigny ; elle se procurait les innombrables brochures politiques et les feuilles publiques auxquelles la Révolution avait donné naissance et lisait tout sans contrôle et sans critique ; ou bien, assise près de sa fenêtre ouvrant sur les vergers de la vallée d’Auge, elle cousait et tricotait des vêtements pour les pauvres du village. Elle resta là près d’un an ; mais à bout de ressources, M. de Corday dut se réfugier chez ses parents, à Argentan. Charlotte refusa de l’y suivre et vint demander asile à une vieille parente, M\up{me} de Bretteville-Gonville, riche et méfiante, qui habitait à Caen un antique et maussade logis de la rue Saint-Jean.

M\up{me} de Bretteville, avec ses quarante mille francs de rente, craignait toujours d’être la victime de quelque escroquerie ; aussi accueillit-elle froidement cette cousine inconnue dont l’air concentré et grave lui faisait peur ; mais cette prévention se dissipa vite, et Charlotte conquit l’affection de la vieille dame. Les souvenirs et les notes manuscrites de quelques contemporains fournissent de précieux détails sur la vie que menait Charlotte dans la maison de la rue Saint-Jean : à vingt-trois ans, elle était très grande et très belle ; son teint avait une transparence « éblouissante » ; elle rougissait avec une facilité extrême ; ses yeux étaient bien fendus et très beaux ; l’expression de son visage et le son de sa voix gardaient une douceur ineffable. Elle habitait une chambrette située à l’extrémité du logis ; pas de parquet, un simple carrelage de briques ; pas de plafond, des solives noircies ; une vaste cheminée et une étroite fenêtre donnant sur une cour obscure. On l’aperçut souvent, à cette fenêtre, calquer des petits dessins qu’elle appliquait sur la vitre. D’ailleurs elle voyait le monde, car M\up{me} de Bretteville recevait beaucoup et fit présent à sa jeune parente de plusieurs jolies robes, dont Charlotte d’ailleurs prenait peu de souci.

Eut-elle des amoureux ? On a étudié la question et conclu par la négative. Il n’existe qu’un court billet, signé de Charlotte, par lequel elle remercie un poète, M. Lecavalier, qui lui avait adressé quelques vers sous le titre \textit{À ma bien-aimée}. La réponse de la jeune fille est tournée en termes polis, mais très froids, et bien certainement les relations entre elle et son adorateur en restèrent là. Il faut cependant mentionner le nom d’un jeune homme de Caen, Franquelin, qui, passionnément épris de Charlotte, aurait obtenu d’elle quelques lettres qu’il conservait jalousement. S’il faut en croire La Sicotière, Franquelin, mort d’une maladie de poitrine peu de temps après l’exécution de son héroïne, fut inhumé dans le cimetière de Vibraye : suivant son désir, on avait placé dans son cercueil les chères lettres dont il ne voulait pas être séparé.

Du logis de la rue Saint-Jean, Charlotte Corday vit passer les Girondins fugitifs : c’est là qu’elle conçut l’effroyable projet obstinément renfermé dans son cœur ; c'est là que, froidement, elle en prépara silencieusement la mise en œuvre ; se procura un passeport ; disposa, à l’insu de M\up{me} de Bretteville, son petit bagage. Tandis qu’elle roulait dans sa jolie tête ces farouches pensées, elle continua de voir le monde, d’aider sa parente à recevoir : nul ne se doutait de ce qui l’occupait. Plus tard seulement, quand on apprit les choses, certains se rappelèrent qu’elle s’était montrée à diverses reprises, plus nerveuse qu’à l’ordinaire ; ainsi, vers le 5 ou 6 juillet 1793, elle se rendit à Verson pour faire visite à sa parente, Mme Gautier de Villers ; elle trouva celle-ci occupée à écosser des pois en compagnie de ses deux servantes. « Je viens te dire adieu, fit Charlotte, j’ai un voyage à faire. » Elle paraissait fort émue. La conversation continua, très banale. Soudain Charlotte prit une poignée de pois en cosses, les froissa rageusement et les jeta à terre ; puis elle se leva, embrassa M\up{me} Gautier à plusieurs reprises et s’enfuit.

En rentrant chez M\up{me} de Bretteville elle traversa l’atelier d’un menuisier nommé Lunel, qui habitait le rez-de-chaussée. Lunel jouait aux cartes avec sa femme ; deux enfants s’amusaient dans un coin de la chambre. Charlotte caressa les petits d’un air songeur ; tout à coup, frappant un grand coup de poing sur la table, elle cria : « Non il ne sera pas dit qu’un Marat a régné sur la France ! » Et elle regagna précipitamment sa chambre, laissant Lunel et sa femme stupéfaits.

Au jour qu’elle avait fixé, elle se glissa dehors, le matin, portant un carton et des crayons, comme si elle allait dessiner dans les prairies. Sur la porte, elle vit le petit Louis Lunel. « Tiens, dit-elle en lui donnant le carton et les crayons, voilà pour toi ; sois bien sage et embrasse-moi... Tu ne me reverras plus. »

Puis elle partit, toute légère, dans la direction du bureau des diligences de Paris. On sait le reste.

\begin{flushright}
\large G. L\textsc{enôtre}, \textit{Sous le bonnet rouge : croquis révolutionnaires,} 1936.
\end{flushright}

% www.biblisem.net

\end{document}
