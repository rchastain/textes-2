\documentclass[a4paper,12pt]{article}

\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{microtype}
\usepackage{geometry}
\geometry{margin=24mm}

\usepackage{lettrine}
\usepackage{csquotes}

%~ \setmainfont[
    %~ Path           = libre-baskerville/,
    %~ Extension      = .otf,
    %~ Ligatures      = TeX,
    %~ BoldFont       = LibreBaskerville-Bold,
    %~ ItalicFont     = LibreBaskerville-Italic
%~ ]{LibreBaskerville-Regular}

%~ https://www.fontsquirrel.com/fonts/libre-baskerville

%~ \setmainfont{LibreBaskerville}

\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\setlength{\columnsep}{6mm} 

%~ \usepackage{lastpage}
%~ \usepackage{fancyhdr}
%~ \pagestyle{fancy}
%~ \fancyhf{}
%~ \fancyfoot[C]{\thepage \hspace{1pt} / \pageref{LastPage}}
%~ \renewcommand{\headrulewidth}[0]{0pt}

\begin{document}

%~ \thispagestyle{empty}

\begingroup  
  \centering
  \LARGE Un témoin du 10 août\\[1em]
  \normalsize par\\[1em]
  \large G. L\textsc{enôtre}\\[2.5em]\par
\endgroup

%~ \section*{\centering Adam, Ève, la famille.}

\lettrine{L}{e} plus humble citoyen peut s’offrir le luxe de transmettre son nom à la postérité et de le rendre quasi immortel. Il n’est pas besoin pour obtenir cet avantage – si c’en est un – de conquérir des provinces ou d’inventer un explosif : il suffit de consigner honnêtement, simplement, sans littérature et sans parti pris, les évènements dont le hasard vous fait le témoin. Barbier, petit bourgeois de Paris, pour avoir, du fond de sa boutique, écrit tous les jours ce que lui racontaient ses voisins, est plus souvent cité, commenté et consulté que l’abbé Velly, auteur d’une histoire de France en 30 volumes, ou que Mézerai, très admiré par ses contemporains et qui put se croire à jamais illustre. Le concierge de l’Opéra de la place Louvois, Roullet, s’est couvert de gloire – et de ridicule, ce qui assure mieux encore la pérennité de sa renommée – en racontant dans une brochure qui a eu les honneurs de la réimpression son rôle de mouche du coche lors de l’assassinat du duc de Berry, en 1820. Le « J’étais là, telle chose m’advint », du bon La Fontaine, prévaut sur tous les plus savants commentaires des chroniqueurs rétrospectifs les mieux renseignés.

La \textit{Revue de Paris} a publié l’un de ces témoignages, émanant d’un gentilhomme qui se trouvait présent au château des Tuileries, lors de la fameuse journée du 10 août 1792. François de La Rochefoucauld, futur duc de Liancourt et d’Estissac, était alors simple officier en disponibilité. Dans sa courte relation, pas de politique, pas de considération sur les causes et les résultats de l’évènement ; rien que le récit d’un homme qui se voit inopinément mêlé à un drame imprévu ; et ces quelques pages sont plus émouvantes dans leur simplicité que les plus emphatiques et même les plus « romancées » des innombrables amplifications auxquelles ce fait historique a donné lieu.

Le 9 août, au matin, François de La Rochefoucauld avait assisté à la messe du roi. L’office ne se célébrait plus dans la chapelle du château, trop éloignée des appartements, mais dans la galerie de Diane. Une peinture qui appartenait, en 1912, à M. Ambroise Loyer, représente cette cérémonie : c’est un document extrêmement précieux, évidemment « pris sur nature ». On y voit la galerie tendue de magnifiques tapisseries, un petit autel portatif, posé à même le parquet, la famille royale agenouillée et, à distance respectueuse, le groupe des courtisans, debout, chapeau sous le bras. François va finir son après-midi à la Comédie. Paris est parfaitement tranquille ; mais parmi les spectateurs certains bruits courent, peu rassurants. Il quitte le théâtre, prend un cabriolet, retourne aux Tuileries afin de s’informer. Tout est calme ; peu de gardes dans les cours, mais, comme à l’ordinaire, d’assez nombreux flâneurs, dont plusieurs femmes. François retourne donc à la Comédie, assiste à la fin de la pièce et, tout de même préoccupé, revient au château où, d’ailleurs, depuis un mois, il a son logement. Il monte chez son ami Tourzel, fils de la gouvernante des enfants de France ; celui-ci le rassure : – Il n’y aura rien, ou « pas grand-chose ». Ils ne soupent point et se font servir du punch.

Vers 11 heures du soir, Tourzel reçoit de sa mère un mot assez inquiétant : la nuit s’annonce mal ; on craint que quelque attroupement ne se porte aux Tuileries ; plusieurs personnes viennent causer avec les jeunes gens : l’idée qui domine, c’est que « le projet » est d’enlever le roi ; on l’a déjà supplié de partir ; il s’y refuse ; mais le parti constitutionnel, l’Assemblée législative, les ministres souhaitent son éloignement et espèrent qu’un mouvement populaire le déciderait à prendre cette résolution. On est toujours étonné de constater, dans les mémoires écrits au jour le jour, combien les contemporains sont ignorants des évènements qui se passent sous leurs yeux : dès qu’ils sortent du petit détail intime, de la simple notation de leurs faits et gestes, ils se leurrent ou s’illusionnent. Ainsi, le soir du 9 août 1792, à minuit, comme sonne la première heure du dernier jour de la monarchie, alors que tous les faubourgs se mettent en marche vers les Tuileries, pour François de La Rochefoucauld et les gentilshommes qui l’entourent, il ne s’agit que d’un départ de Louis XVI pour Saint-Hubert ou Fontainebleau ; ce qui confirme cette hypothèse, c’est le bruit répandu dans le château qu’il n’y aura pas de « coucher du roi ». C’est la première fois que pareille infraction est faite à l’étiquette, et ceci trouble les gens de cour plus que tout le reste. Pas de coucher du roi ! Ça c’est grave, et voilà tout le monde botté et prêt à suivre Sa Majesté. – Où ? On n’en sait rien. François eut, toute la nuit, un cheval qui l’attendit au bord de l’eau, près du pont Louis XVI (le pont actuel de la Concorde).

Personne ne se couche ; La Rochefoucauld monte à l’appartement du roi. Beaucoup de monde ; très peu de gens « habillés » ; il y en a de tous les partis, même des fonctionnaires publics ceints de leurs écharpes. Voilà Pétion, le maire de Paris, « la tête haute, le regard faux » ; il traverse la foule ; nul ne se range sur son passage ; mais il prend soin de ne bousculer personne. Le roi lui parle avec autorité et Pétion promet qu’il repoussera, s’il y a lieu, la force par la force. Les heures passent ; on est harassé de piétiner dans ces grandes pièces étouffantes. On n’a pas le droit de s’asseoir dans les appartements du roi, mais tout le monde est si las qu’on s’installe comme on peut : les uns s’assoient par terre, les autres sur les tables, sur les consoles, partout ou l’on trouve à s’appuyer et à soulager ses jambes ; les plus audacieux prennent possession des fauteuils, au grand scandale des huissiers qui, dans l’immense désastre menaçant, essayent encore de sauvegarder l’étiquette. Vers trois heures, on entend tinter le tocsin. Louis XVI, au petit jour, descend au jardin, accompagné de quelques fidèles ; il visite les postes, pousse jusqu’au pont tournant (aujourd’hui la grille entre les deux terrasses, place de la Concorde) ; il a l’air « peiné et inquiet » et s’efforce « de paraître serein ». Des factieux à mauvaises figures tentent de l’approcher ; mais ses courtisans, se prenant par le bras, forment « une chaîne » autour de lui et le préservent de tout contact. Un grenadier, bien intentionné, lui dit : « Sire, vous devriez retourner au château au plus vite. » Louis XVI répond : « Il est singulier que j’aie moins peur qu’un brave grenadier. » Il regagne cependant, très entouré, le pavillon central du palais et remonte à ses appartements.

Si ce très intéressant mémoire doit paraître en volume, il serait bien désirable qu’on y joignît un plan des appartements royaux : la topographie, disait un sage, est l’un des deux yeux de l’Histoire ; l’autre est la chronologie. Or La Rochefoucauld nous promène dans les Tuileries en homme qui en est l’hôte familier, et sans se douter qu’un temps viendra où ses indications sommaires seront pour les lecteurs lettre morte. La minutieuse exactitude de sa relation se vérifie pour qui en suit les péripéties sur un plan de l’époque : afin de la bien goûter, il faut savoir que le premier étage du grand pavillon central des Tuileries formait un vaste vestibule (ce sera, sous Napoléon III, la salle des Maréchaux). Ensuite on traversait, en se dirigeant vers le pavillon de Flore, la salle des Suisses, la salle de l’Œil-de-Bœuf, et on atteignait la chambre du lit. À partir de là, le corps de bâtiment était double : sur la cour du Carrousel donnaient, en enfilade, après la chambre du lit, le cabinet du roi et la galerie de Diane ; sur le jardin prenaient jour les appartements privés du roi, qu’habitaient aussi le dauphin et sa sœur. La reine occupait le rez-de-chaussée de cette même partie du château et Madame Élisabeth était logée au pavillon de Flore.

À son insu, bien certainement, François de La Rochefoucauld nous apporte la solution définitive d’une énigme qui intriguait depuis longtemps les historiens de la Révolution. Parmi les plus fidèles défenseurs de la royauté moribonde, il rencontre, dans la nuit, aux Tuileries son cher ami Lazowski ; il le connaît depuis longtemps ; ils ont ensemble accompagné Arthur Young, le célèbre agronome anglais, dans son voyage à travers la France. Young a même séjourné à Lunéville chez Lazowski, auquel le duc de Liancourt a confié l’éducation de ses deux fils. François qui l’a eu pour mentor ne peut donc se tromper ni sur la personnalité, ni sur les sentiments du personnage. Mais les journaux et les récits du temps, et depuis lors bien des historiens, mentionnent au contraire Lazowski comme ayant entraîné à l’attaque du château la populace parisienne, dont il commandait l’artillerie. Violent sans-culotte, massacreur de septembre, associé du menuisier Duplay dans l’entreprise d’imprimerie installée près du logis de Robespierre, Lazowski fut à ce point adoré par les jacobins que, lorsqu’il mourut, en avril 1793, ils l’inhumèrent sur la place du Carrousel afin d’honorer sa démagogie et son intrépide civisme. Quoique la palinodie ne soit pas rare en temps de révolution, on ne s’expliquait pas la conduite d’un homme qui, zélé défenseur des Tuileries à onze heures du soir, les canonnait à six heures du matin. Aujourd’hui tout s’éclaire : les Lazowski étaient deux, – deux frères : l’aîné, l’ami de La Rochefoucauld, le protégé du duc de Liancourt, se prénommait Maximilien ; le cadet, Claude, ex-inspecteur des manufactures, fut le sans-culotte un instant fameux. Ils se ressemblaient étonnamment sans doute puisque M\up{me} Roland les a confondus, entraînant dans son erreur nombre de biographes. D’ailleurs, M. Pierre Boyé, dans son histoire de la Cour polonaise de Lunéville, a donné de la généalogie compliquée des Lazowski un tableau qui ne laisse aucun doute.

Reprenant le récit de La Rochefoucauld, on verra que ce gentilhomme accompagnait la famille royale lorsqu’elle abandonna les Tuileries pour se réfugier au Manège où siégeait l’Assemblée législative. Il est là, qui donne le bras à M\up{me} de Lamballe, très émue et tremblante. La contenance du roi est assurée ; la reine est tout en pleurs et, de temps à autre, essuie ses larmes, « s’efforçant de prendre un air radieux qu’elle conserve quelques minutes » ; le petit dauphin n’a pas l’air très effrayé ; sa sœur pleure à gros sanglots.

La Rochefoucaud conduisit M\up{me} de Lamballe et Madame Élisabeth, résignée et courageuse, jusqu’à la tribune de l’Assemblée. Il les revit, vers le soir, dans la loge du Logographe où toute la famille royale était confinée. Le roi, « avec un visage abattu et fatigué », occupait le devant de la loge, suivant au moyen de sa lorgnette les divers orateurs qui se succédaient à la tribune. Près de lui était la reine « dont les larmes et la sueur avaient entièrement mouillé le fichu et le mouchoir » ; elle avait sur les genoux le dauphin qui dormait. La Rochefoucauld offrit ses services : Marie-Antoinette le pria de lui procurer un mouchoir de rechange. Il n’en avait pas, le sien ayant servi à panser un blessé ; il se mit donc en quête ; un tenancier de la buvette consentit à lui en prêter un ; mais quand il se présenta pour le remettre à la reine, il ne put approcher de la loge ; les sentinelles menaçaient de l’arrêter. Il s’enfuit par les couloirs sombres qui régnaient sous les gradins de l’Assemblée, revint au buvetier qui l’aida à se déguiser. Le lendemain La Rochefoucauld quittait Paris, que de huit ans il ne devait plus revoir.

\begin{flushright}
\large G. L\textsc{enôtre}, \textit{Sous le bonnet rouge : croquis révolutionnaires,} 1936.
\end{flushright}

% www.biblisem.net

\end{document}
