\documentclass[a4paper,12pt]{article}

\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{microtype}
\usepackage{geometry}
\geometry{margin=24mm}

\usepackage{lettrine}
\usepackage{csquotes}

%~ \setmainfont[
    %~ Path           = libre-baskerville/,
    %~ Extension      = .otf,
    %~ Ligatures      = TeX,
    %~ BoldFont       = LibreBaskerville-Bold,
    %~ ItalicFont     = LibreBaskerville-Italic
%~ ]{LibreBaskerville-Regular}

%~ https://www.fontsquirrel.com/fonts/libre-baskerville

%~ \setmainfont{LibreBaskerville}

\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\setlength{\columnsep}{6mm} 

%~ \usepackage{lastpage}
%~ \usepackage{fancyhdr}
%~ \pagestyle{fancy}
%~ \fancyhf{}
%~ \fancyfoot[C]{\thepage \hspace{1pt} / \pageref{LastPage}}
%~ \renewcommand{\headrulewidth}[0]{0pt}

\begin{document}

%~ \thispagestyle{empty}

\begingroup  
  \centering
  \LARGE À Versailles\\[1em]
  \LARGE LES PREMIÈRES HEURES DE LA RÉVOLUTION\\[1em]
  \normalsize par\\[1em]
  \large G. L\textsc{enôtre}\\[2.5em]\par
\endgroup

%~ \section*{\centering Adam, Ève, la famille.}

Depuis 1789, l’incomparable château de Versailles n’est plus qu’une grande demeure morte. Il semble qu’il ne se soit jamais remis d’avoir été le théâtre des premiers troubles révolutionnaires. Dans ces galeries et ces salons d’apparat, si bien créés pour le faste, où l’esprit n’imagine que des défilés de cortèges, circulant à pas réglés comme une mise en scène de l’Opéra ou des réceptions solennelles et compassées, avec révérences étudiées, gestes de théâtre et attitudes cérémonieuses, on évoque malaisément l’aube sanglante de la Révolution.

Peut-être est-ce d’ailleurs cette légende si fausse d’une Cour uniquement préoccupée de plaisirs et de fêtes qui déchaîna contre elle un peuple affamé et malheureux. Le Versailles royal fut tout autre. Saisissons, avant la tragédie, les dernières images d’un règne qui, en quelques heures, allaient être anéanties.

\vspace{1cm}
\centerline{\rule{0.3333\linewidth}{.2pt}}
\vspace{1cm}

\begingroup  
  \centering
  \normalsize VERSAILLES SOUS LOUIS XVI\\[2.5em]\par
\endgroup

Encore que nous soyons fort peu renseignés sur le « trantran » quotidien du Château et sur l’existence de ses innombrables hôtes, les Mémoires des contemporains nous en apprennent assez pour dérouter notre imagination et pour nous montrer que, au temps de Louis XVI tout au moins, la Cour de France était parvenue à prendre ses aises avec ce trop splendide décor et se comporter en ce palais des Mille et une nuits comme dans la plus bourgeoise des demeures.

D’abord, point de gardes ; à peine, çà et là, quelques soldats suisses postés en sentinelles, plutôt pour renseigner le visiteur que pour le surveiller ; les portes ouvertes, à tout venant, noble ou bourgeois ; l’étiquette exige bien qu’on ne se présente dans les appartements du Roi qu’avec l’épée au côté et le chapeau tricorne sous le bras ; mais ce règlement est tombé en désuétude ; d’ailleurs le concierge de l’aile de la Chapelle loue des épées à ceux qui en désirent ; la plupart des visiteurs s’en dispensent et n’en circulent pas moins d’un bout à l’autre du grand appartement, sans que quiconque s’étonne ou s’inquiète.

La Galerie, la somptueuse Galerie des Glaces, avec l’Olympe de son plafond, ses dieux antiques et ses pilastres de marbres, – la plus belle salle, sans contredit, qui soit au monde, – la Galerie est un simple passage ; jusqu’au milieu du jour on y voit défiler des porteurs d’eau et des monteurs de bois ; on y rencontre des laquais partant en course, et des soubrettes en commission ; des provinciaux bouche bée, des petits boutiquiers parisiens avec leur femme, leurs mioches et leur bonne, venus là en partie de plaisir et s’attardant, panier au bras, dans ces splendeurs, avant d’aller s’installer sur l’herbe sous quelque ombrage du parc pour y déjeuner à l’air. On y voit même passer, sur les parquets cloisonnés, des bestiaux ; vaches, chèvres, ânesses, sont conduites jusque dans les appartements des princes et des princesses pour y donner leur lait... Parfois, un arrêt se fait dans cet incessant va-et-vient ; l’une des portes de glace s’ouvre ; les gens s’appellent, accourent, se groupent... C’est le Roi qui sort de chez l’un de ses frères, soit qu’il aille à sa chapelle ; il passe, bonhomme et souriant, tout simple, escorté de quelques gentilshommes avec lesquels il cause familièrement ; il est vêtu de gris ou de marron, à son habitude ; et les bourgeois et les petits boutiquiers s’extasient : – « Il a ri ; avez-vous vu qu’il a ri ? » On en parlera durant huit jours. Si on le suit jusqu’à la chapelle, on le revoit là, tout aussi peu emphatique et guindé : même aux jours de grand office, il n’affecte aucune pose ; dans cette superbe tribune à balustre de bronze, à hautes portes d’or, à mosaïque de marbre, sur son prie-Dieu et son fauteuil imposants comme des trônes, il est chez soi, tout à son aise ; il vide ses poches, pose sur la balustrade son gros livre de prières, ses lunettes, son mouchoir, s’installe, chante sa messe sans fausse honte ; et quand on lui présente le pain bénit, sous la forme d’une brioche de forte taille, Louis XVI tire de sa poche son couteau, l’ouvre et se coupe une tranche du gâteau ; souvent même il ne prend pas tant de peine et il mord à même la brioche.

Malgré ces aperçus, trop rares, sur la vie du Versailles d’autrefois, il faut bien nous résigner à reconnaître qu’elle reste pour nous, comme pour l’histoire, absolument mystérieuse : car je ne vois pas comment on peut expliquer ces surprenants Comptes des Petits appartements, tenus par le Roi lui-même et qu’a publiés, d’après le manuscrit original, M. le Comte de Beauchamp. Il est malaisé d’admettre que le Roi fût informé des menues dépenses que, à l’exemple d’une pauvre ménagère tenant son carnet de comptes, il prend chaque jour la peine d’inscrire de sa propre main. Il marque les œufs frais achetés pendant le mois, les pourboires du porteur d’eau, le prix de ses ports de lettres, le linge remis à la blanchisseuse (– 49 nappes et 438 serviettes en juin 1775). Il note les carafes cassées, – à 5 sols l’une, – et on en casse ! – 249 en ce même mois de juin, – 545 en juillet ! Voici, en septembre, « deux harengs frais » – « une corde pour le tournebroche de Fontainebleau » – puis
« six paniers de grosses cerises », – pour des confitures, évidemment ;
– « des brosses à vaisselle et une livre de savon », – 4 livres 10 sols pour « des pieds de mouton et du gras double », – « une bouteille de vin rouge pour une matelote », et ce qui paraît plus extraordinaire encore :
– « fleurs pour la table – 48 livres en un mois ». Le Roi achetait des fleurs ! Le Roi de Versailles, de Trianon, de Saint-Cloud, de Marly, dont les jardins étaient fameux dans le monde entier, avait recours à la fleuriste quand il voulait offrir un bouquet à la Reine.

Chez celle-ci, pas plus de décorum ; mais comme il convient, beaucoup plus de discrétion dans la familiarité. N’empêche que chez elle aussi, le premier manant venu entre comme il lui plaît : le peuple est chez lui dans le château des rois, et si l’on en doutait, je renverrais les incrédules à ce récit d’un voyage à Paris qu’a tracé un faiseur de bas d’Avignon, qui, visitant Versailles dans l’été de 1780, se promène librement dans la chambre du Roi, dans les cabinets, dans les salons et pénètre dans celui de la Reine, où il reste près d’une heure, la regardant jouer aux cartes, sans que personne ne songe à demander au brave Avignonnais ce qu’il fait là et comment il s’y est introduit.

Je pense que s’il existait quelque guide de Versailles, où, après la description et l’histoire obligées des œuvres d’art que renferme le château, pourrait se lire quelque anecdote nous ressuscitant ainsi, sous leur vraie figure, les fantômes du merveilleux palais, la visite en serait plus instructive et moins fatigante. En même temps que leur demeure, restée debout par miracle, je souhaiterais qu’on nous les fît vivre, ces gens de jadis ; j’imagine des « promenades-conférences », où, dans la chambre de Louis XV, par exemple, ce chef-d’œuvre de grâce et de goût, le guide ne manquerait pas de mentionner aux visiteurs : – « Voici la cheminée, où, pendant l’hiver, le Roi, levé de bonne heure, les pieds nus dans ses pantoufles, et enveloppé de sa robe de chambre, allumait lui-même son feu, soufflait sur les tisons et échafaudait les rondins, pour ne pas réveiller ses domestiques et les laisser dormir tout leur soûl. » Et sur la terrasse du château, en cet endroit où les touristes ébahis de tant de beautés harmonieuses, se prennent à réfléchir et s’indignent à la pensée que tout cela a été créé pour la jouissance d’un seul homme, et que le pauvre peuple de ces temps d’oppression n’en a jamais joui, je voudrais qu’il se trouvât là quelqu’un pour raconter l’aventure de ce jeune Tourangeau, frais débarqué de sa province, qui, curieux de visiter Versailles, est venu y passer une journée dans l’été de 1785, en compagnie d’une jeune femme, sa compatriote, dont l’allure nonchalante et la taille arrondie ne laissent aucun doute sur de prochaines espérances de maternité. Vers le soir, les deux provinciaux flânent sur la terrasse, parmi une grande affluence composée de gens « de tous les mondes » ; il y a même des bateleurs et des faiseurs de tours. Mais la promeneuse est excédée de fatigue : où se reposer ? Tous les bancs de marbre sont occupés. Enfin le jeune homme en avise un sur lequel deux femmes seulement sont assises ; il s’élance et va s’emparer de l’espace resté libre à côté d’elles ; il en prend, sans cérémonie, possession, fait signe à sa compagne, jette un regard sur sa voisine. C’est la Reine ! Le voilà aussitôt debout, il salue, s’excuse, expose les motifs de son intrusion ; et Marie-Antoinette insiste pour qu’il aille au plus vite chercher celle à qui est destinée la place si vaillamment conquise. Au moment où la dame, fort émue, va s’asseoir, la Reine fait signe à un heiduque qui passe, lui ordonne de courir jusqu’aux appartements et d’en rapporter un coussin qu’elle dispose elle-même sur le banc, disant : – « Ce marbre serait trop frais pour vous en ce moment, Madame. » Et, la promeneuse enfin installée, la conversation s’engage, aussi simple et familière qu’entre campagnards qui prennent le frais devant leur porte. Vous trouverez l’anecdote dans \textit{Mes récapitulations} de Bouilly, et c’est lui qui en fut le héros.

\vspace{1cm}
\centerline{\rule{0.3333\linewidth}{.2pt}}
\vspace{1cm}

\begingroup  
  \centering
  \normalsize LES PREMIÈRES HEURES DE LÀ RÉVOLUTION\\[2.5em]\par
\endgroup

Le peuple de Paris connaissait donc le chemin de la demeure royale où, si souvent, il avait été admis, où, à chaque fête heureuse, naissance ou mariage de prince, on l’accueillait en enfant privilégié. En l’automne de 1789, alors que les États Généraux siégeaient déjà depuis quatre mois, comme il s’étonnait que les députés n’eussent pas encore réussi à instaurer l’âge d’or, il conçut de la méfiance. Quelqu’un lui souffla que la Cour, la Reine surtout s’opposaient à la réalisation du bonheur promis. Alors il fut pris de colère et résolut d’imposer sa volonté. C’était le 5 octobre de cette année-là. Vers quatre heures de l’après-midi, on aperçut, des fenêtres du château, dans la brume de la grande avenue, l’avant-garde des femmes qui s’avançaient, précédant la cohue de la populace parisienne. Elle marchait à l’assaut de la monarchie.

Ce fut aussitôt, du bout à l’autre du Palais, un grand désarroi. On s’avisa subitement que la demeure royale, faite en vue de la représentation et des fêtes, n’était nullement disposée pour soutenir un siège et que l’accès en était de toutes parts large ouvert. Tandis que la nuit tombait, l’invasion parisienne, silencieusement, comme effrayée de son audace sacrilège, prenait possession de la grande cour et se pressait, à chaque instant plus dense, contre les grilles, dorées mais fragiles ; sans cesse renforcée de nouveaux arrivants, elle moutonnait dans l’immense place, écuries royales, à perte de vue. Une simple poussée de cette formidable horde livrerait à l’émeute le château des rois, à peine défendu par une garnison de parade, – une demi-compagnie de gardes du corps, un détachement de Suisses et une partie du régiment de Flandre. On barricada en hâte quelques issues, et des portes qui, depuis le temps de Louis XIV, n’avaient pas tourné sur leurs gonds, se fermèrent pour la première fois.

À l’intérieur du château, c’était l’angoisse ; les nouvelles apportées du dehors étaient effrayantes ; le bruit s’affirmait que, derrière les premières bandes de manifestants, arrivait en colonnes serrées tout le peuple de Paris, suivi de la Garde nationale avec ses canons. Ce flot, incessamment déferlait contre le palais et se tassait là, menaçant ; de cette cohue stagnante, hérissée de piques, de bâtons et de baïonnettes, s’élevaient dans la nuit sinistre, de grandes rumeurs, coupées de cris, de coups de feu, de menaces à l’Autrichienne et de chansons sanguinaires. Les deux ailes du château, occupées par les bureaux des ministères, étaient déjà envahies ; aux fenêtres apparaissaient, animées et grimaçantes, d’horribles figures de mégères aux bras nus, d’hommes barbus, de bouchers à bonnets de laine, tous hurlant, tendant les poings vers les façades de la Cour de Marbre, si nobles et si fières avec leurs balcons dorés, leurs bustes antiques, leurs statues symboliques représentant « quelques-unes des vertus de Sa Majesté », et son cadran dont les aiguilles étaient depuis quinze ans arrêtées, suivant l’étiquette, sur l’heure de la mort du précédent Roi. Dans les salons, si ordonnés d’ordinaire, si solennels de grandeur et d’aspect, règnent l’affolement et la confusion : à travers la Galerie circulent des gentilshommes désœuvrés, des dames inquiètes : on s’interroge du regard ; on se questionne à voix basse : – Que va-t-on faire ? – Que décide le Roi ? M. de Saint-Priest, ministre de l’Intérieur et M. de la Tour du Pin, gouverneur du château, se sont jetés aux pieds du souverain, le conjurant de se réfugier à Rambouillet ; les derrières du Palais, la terrasse, la route de Saint-Cyr sont libres, et l’on pourrait fuir sans danger. Louis XVI hésite, ne répond rien, ou balbutie : – « Je ne veux compromettre personne. » Enfin il consent : on va partir. – Non, il y a contre-ordre ; on reste. Louis XVI ne veut plus rien entendre : il boude ; il s’est enfermé dans son appartement et refuse d’en sortir. Et la Reine ? Elle a déclaré qu’elle ne quitterait pas son mari...

La nuit, au dehors, est complète ; dans le grand Salon de la Paix, première pièce des appartements de la Reine, sont réunies les femmes de Sa Majesté ; elles attendent des ordres, les unes assises sur des tabourets, les autres sur des tables de jeu. Quelques bougies éclairent à peine la haute salle. Et l’émeute ? Elle grossit toujours ; elle s’écrase contre les murs du Palais ; de temps à autre, pour assouvir sa rage, elle happe, du bout d’une pique, l’un des gardes du corps ou l’un des soldats postés aux portes, l’attire, le désarme, l’engouffre, le déchire ; alors monte un grand cri de triomphe, et bientôt on voit surgir, au-dessus du remous, une face livide et sanglante, hissée à la pointe d’une pique... C’est la tête de la victime dont la foule se fait un drapeau.

Dans l’Œil-de-Bœuf, dans la Chambre de parade, dans le Cabinet du Conseil, dont les fenêtres donnent sur cette houle, les ministres, les courtisans, les conseillers d’occasion sont réunis. La Reine est là, également, avec ses enfants ; le Roi aussi a reparu ; mais il ne dit mot et semble frappé de stupeur ; à chaque instant il se retire dans son cabinet, pour s’y reposer et respirer à l’aise. À qui lui parle il ne répond pas. Le maître n’ordonnant rien, nul n’ose élever la voix ni prendre une résolution. On attend.

Enfin l’espoir renaît, vers minuit : La Fayette, l’idole du peuple, le roi de Paris, fait son entrée ; comme toujours il est sémillant, très calme, presque souriant. Rassurant surtout : il répond de tout ; sa Garde nationale a pris possession des postes ; la nuit sera tranquille, il s’en fait garant ; d’ailleurs il pleut et l’émeute se disperse. De fait, dans les cours, la foule est clairsemée ; elle traîne par les rues voisines du château, en quête d’abris. Dans les pavillons des ministres, des groupes se sont installés ; il y a des dormeurs partout, dans les bureaux, dans les cuisines, sur les marches des escaliers ; on en distingue même étendus sur les pavés boueux. C’est fini, tout danger est passé ; on respire. Le Roi, qui a sommeil, congédie ses courtisans ; les huissiers parcourent la Galerie annonçant que la Reine s’est retirée ; les portes se ferment, les bougies s’éteignent ; chacun regagne son appartement.

Rentrée dans sa chambre, toute voisine du Salon de la Paix, Marie-Antoinette, tranquillisée, a conseillé à ses deux « dames » de se coucher et de dormir ; elles n’ont pas obéi ; avec deux femmes de chambre, elles se sont assises contre la porte du salon ; toutes quatre veilleront jusqu’au matin. Un garde du corps est en faction dans l’antichambre. Tout est calme ; les heures s’écoulent ; l’aube commence à poindre. À ce moment, l’une des femmes de la Reine entend un bruit de pas qui semble monter de la Cour des Princes. Qu’est-ce ? Une patrouille, sans doute. Presque aussitôt la porte de l’antichambre est brusquement ouverte. Le garde crie : « Sauvez la Reine ! » Et il referme aussitôt le battant. Un grand tumulte, la chute d’un corps, des heurts brutaux, l’épouvante. Comme un ouragan, l’émeute réveillée se rue, à grandes enjambées, dans l’escalier de marbre ; la Salle des Gardes est envahie. À demi-nue, la Reine a sauté de son lit – ce grand lit d’apparat avec son dais drapé de lampas, dont on voit encore, dans la corniche du plafond, les solides pitons, – elle s’enfuit par les petites pièces qui séparent son appartement de la salle de l’Œil-de-Bœuf, se heurte à une porte barricadée, appelle, se fait connaître...

Tout le décor du drame existe encore, presque intact. Je ne sais si, actuellement, les exigences du service permettent d’ouvrir aux visiteurs du Palais ces étroits cabinets que traversa, ce matin du 6 octobre 1789, la Reine éperdue. Mais, du salon de l’Œil-de-Bœuf, il est facile d’y jeter un regard : un couloir exigu, une antichambre très basse de plafond, puis laissant sur la gauche les délicieux salons formant les petits appartements créés pour Marie-Antoinette, on est à la porte qui s’ouvrait, dans la grande chambre, à droite du lit de la Reine. On voit même là, au travers d’un châssis vitré, l’enfoncement du petit escalier, reste du « passage du Roi », par lequel, aux jours heureux, Louis XVI se rendait chez sa femme. Voici la porte, fleurie d’ornements dorés, où tomba le garde du corps, M. de Miomandre, qui, d’ailleurs, survécut à ses blessures ; sur ce parquet se sont posés, ce matin-là, les pieds nus de la Reine ; sa main tremblante a touché ce bouton de serrure. Et, plus loin, dans la Chambre de parade du Roi, est le balcon de fer doré où La Fayette parut, dans la matinée, aux côtés de l’Autrichienne, que réclamait à grands cris la populace reprise de rage. Je ne sais si La Fayette était ému, ou s’il manquait naturellement d’éloquence ; mais la harangue qu’il adressa au peuple en cette circonstance mémorable fut singulière : la voici textuellement rapportée par des témoins indiscutables : – « La Reine est fâchée de voir ce qu’elle voit devant ses yeux ; elle a été trompée ; elle promet qu’elle ne le sera plus... Elle promet d’être attachée au peuple comme Jésus-Christ à son Église...! » La Reine, près de « l’orateur », pleurait, – d’émotion ou de honte : elle leva deux fois la main comme pour prendre à témoin le ciel de la sincérité de son serment...

Dans la chambre du Conseil, nommée ordinairement le grand cabinet du Roi, Louis XVI reçut la députation des femmes venues pour lui demander « du pain ». N’imaginez point un groupe de mégères pénétrant insolemment jusqu’au monarque et lui parlant d’un ton comminatoire. Ce fut tout autre chose et l’on peut reconstituer la scène grâce à la minutieuse enquête entreprise par les magistrats du Châtelet sur ces journées tragiques. En venant à Versailles, la foule souhaitait bien certainement pénétrer dans le château et faire entendre au Roi ses doléances ; mais, soit crainte d’une résistance, soit respect instinctif pour la majesté royale, on décida que quelques femmes seulement porteraient la parole au nom du peuple. Qui les élut ? On ne sait pas. Dix furent désignées et parvinrent à traverser la bousculade jusqu’aux portes du vestibule de marbre dont la garde leur interdit l’entrée. Elles s’étaient groupées là, refusant de s’en aller ; elles attendirent longtemps ; enfin, comme la porte s’ouvrait devant « quelques messieurs vêtus de noir, qu’on disait être des députés convoqués par le Roi », les dix femmes s’accrochèrent à leurs bras et passèrent avec eux. Les voilà montant l’escalier pompeux, traversant les vestibules, arrivant à l’antichambre ; mais là, porte close ; « un seigneur habillé de bleu avec passepoils rouges » déclare que Sa Majesté tient conseil et ne peut recevoir. Elles restent là, ébahies de se trouver dans ce sanctuaire, parmi les courtisans, les huissiers et les gardes. L’une d’elles, Victorine Sacleux, teinturière de son état, et qui, en se démenant dans la foule, a été blessée d’un coup d’épée à la main, s’évanouit ; on l’emporte ; les autres s’obstinent. Enfin « un Monsieur, qu’on appelle M. de Guiche », annonce que quatre de ces femmes seulement seront admises auprès du Roi, et les huissiers laissent passer les quatre qui se trouvent le plus près de la porte : l’histoire a conservé le nom de trois d’entre elles : une ouvrière en sculpture, Louison Chabry, âgée de dix-sept ans ; une bouquetière de vingt ans, Françoise Rolin, et une dentellière, Rose Barré ; celle-ci boite, ayant un pied enflé par suite de la marche. M. de Guiche les repasse à M. le Comte d’Affry qui les remet à M. de Saint-Priest, toutes tremblantes et presque épouvantées de ce qui leur arrive ; par l’Œil-de-Bœuf, elles pénètrent dans la Chambre de parade où elles s’arrêtent, ébahies d’admiration, devant un lit vaste et somptueux comme un reposoir ; quelqu’un leur dit que c’est le lit de Louis XIV.

À la porte de la salle du Conseil, Françoise Rolin, essayant de se faufiler, bouscule un suisse qui la repousse si brusquement qu’elle tombe et reçoit « plusieurs coups de pied ». Pendant qu’elle se relève en pleurant, ses camarades ont déjà disparu. Un gentilhomme, M. d’Estaing, la console : – « Tu pleures parce que tu ne vois pas le Roi ? » dit-il ; et il la fait entrer dans « un salon » ; autour d’une table couverte d’un tapis vert, beaucoup de seigneurs sont debout. Il semble bien, d’après sa déposition, qu’elle ne se doute pas qu’elle est devant le Roi. Louise Chabry répond en balbutiant aux questions de Louis XVI, qui lui parle « avec beaucoup de bonté ». Il lui demande « si elle veut du mal à la Reine ». Elle répond non, pâlit, soupire et perd connaissance. Le Roi fait apporter un verre de vin « dans un gobelet d’or » ; elle revient à elle, il la réconforte, la congédie « par d’aimables paroles » ; et sans savoir par où on l’a conduite, elle se retrouve dans la cour ; la foule, la voyant reparaître, l’entoure ; on veut savoir. Qu’a-t-elle fait ? Qu’a dit le Roi ? Elle raconte : il est très bon ; il a été très gentil pour elle ; alors c’est un tumulte, on l’insulte, des femmes crient qu’elle a trahi, qu’on l’a payée ; on se rue sur elle ; des mains brutales lui passent au cou une jarretière, tandis que deux maraîchères, qu’elle reconnaît, – Rosalie et la grande Louison, – la poussent vers une lanterne pour la pendre. Des soldats la dégagent ; elle doit remonter chez le Roi qui l’accueille de nouveau et consent à se montrer avec elle au balcon pour déclarer qu’elle n’a pas reçu un sol ; puis il ordonne qu’on la reconduise à Paris dans une de ses voitures.

Ainsi le destin voulut que la dernière femme « présentée » à la Cour et, suivant la vieille étiquette « dans le grand cabinet du Roi », fût une fille du peuple, une simple ouvrière, venue là pour demander du pain.

\vspace{1cm}
\centerline{\rule{0.3333\linewidth}{.2pt}}
\vspace{1cm}

\begingroup  
  \centering
  \normalsize LA FIN DE VERSAILLES\\[2.5em]\par
\endgroup

Car c’en était fini de Versailles. Le jour même, la famille royale était traînée à Paris ; l’immense château dont elle était l’âme resta pour toujours inhabité. Qui ne connaît la page superbe de Goncourt, explorant, guide de l’époque en main, la demeure royale désertée : – « Montez le grand escalier, silence et nudité ! Galeries muettes ! Palais mort... Vendu le mobilier ; le grand meuble en gros de Tours broché, fond blanc !... dessin de fleurs, plumes de paon et rubans noués, frangés de soie ! Vendu le grand meuble d’été à bouquets détachés et guirlandes formant mosaïque, soutenu par un courant de feuillage en or... »

« Et c’est alors qu’on voit chez des savetiers de Versailles des lits de satin à baldaquins d’or. Ces tapisseries, cet or, ces pierreries et ces milliers de cristaux qui reflétaient l’incendie des bougies, où sont-ils ? – Dans le sérail du dey d’Alger ! À la voûte du salon de la Guerre, que sont devenus les globes avec les armes et les couronnes de France, et la devise de Louis XIV entre des trophées de stuc doré ? Brisés ! Et la grande galerie où Lebrun a peint l’allégorie de Louis-le-Dieu ? – L’humidité a fait ravage : voyez, les traits de Turenne sont effacés ; baissez-vous ; vous ramasserez à terre les écailles du Passage du Rhin ou de la Jonction des deux mers. Par là, – les petits appartements, – tout raconte Octobre. Prêtez l’oreille : une sourdine de flûtes et de harpes ; puis des tons mourants... C’est l’horloge restée dans la chambre de la Reine, immuable, et qui, de ses sonneries royales a chanté toutes les heures de la Révolution... »

Ces ventes sacrilèges se prolongèrent du 10 juin 1793 au 11 août 1794 : leur procès-verbal mentionne 17.182 lots, meubles, literie, glaces, pendules, lustres, voitures, vins, etc.

Le 12 mars 1796 ça recommença avec les mobiliers de luxe, et à cette seconde vente la France perdit pour des dizaines de millions de merveilles d’art, commodes, secrétaires, guéridons, bureaux, encoignures en bois d’acajou, en bois satiné, en bois de rose, en bois de citron, plaqués de laque de Chine, ornés de bronzes ciselés et dorés, décorés de médaillons d’oiseaux et de papillons, de camées, de porcelaines blanches et bleues à paysages et figures d’après Watteau... Tout cela fut cédé pour quelques poignées d’assignats, qui, à ce temps-là, valaient à peu près six sous pour cent francs !

Le crime accompli, on installa dans le Palais dévasté l’École centrale de Seine-et-Oise, que remplacèrent les Invalides... Ce fut le salut pour le château du grand roi, qui, s’il n’avait pas été utilisé, tant bien que mal, n’eût pas échappé certainement au vandalisme révolutionnaire. Grâce à ses écoliers et à ses Invalides, grâce surtout à l’intervention réitérée des Versaillais, les murs, du moins, furent préservés. En 1795 on proposa d’installer là le Directoire ; mais les Cinq eurent le bon goût de comprendre que leur petitesse contrasterait défavorablement avec la grandeur des souvenirs qu’on voulait abolir.

Le Premier Consul qui, lui, ne craignait aucune comparaison, s’occupa à sauvegarder ce qui restait. Il s’en occupa même trop ; car, devenu empereur, et ne trouvant pas la demeure de la monarchie à sa taille, il rêvait de tout jeter bas, pour reconstruire « en mieux », et il se trouva un architecte, qui, pour la bagatelle de 500 millions, se chargeait de l’opération !

Heureusement, on ne fit rien et le château resta inoccupé. La Restauration y logea quelques vieux émigrés sans abri : de pauvres hères à grand nom, mais qui n’avaient pas de bonne, installèrent leurs ménages dans les salons de Louis XIV ; des tuyaux de poêle sortaient des fenêtres et des lessives séchaient sur les balcons.

Viollet-le-Duc racontait que, au temps de sa jeunesse, visitant le palais en compagnie d’une vieille marquise, familière de l’ancienne Cour, il s’étonnait qu’elle n’y reconnût rien et semblât perdue dans cette merveille pompeuse déchue au rang de maison garnie et distribuée à la fantaisie de ces locataires d’occasion, en une série de petites pièces, coupées de cloisons, d’entresols bas, de cuisines sans jour et sans air, de débarras sordides, de véritables campements installés dans les galeries et jusque sur les escaliers.

Une population hétéroclite s’était ainsi taillé un manteau d’arlequin dans la magnifique pourpre d’Hercule ; le château de Versailles allait à sa ruine.

De nouveau on projetait de l’abattre avant qu’il s’écroulât de vétusté. Qui eût prédit alors que l’avenir lui réservait des heures de gloire auprès desquelles devaient pâlir les plus belles pages de son passé ?

\begin{flushright}
\large G. L\textsc{enôtre}, \textit{Sous la Révolution,} Flammarion, 1937.
\end{flushright}

% www.biblisem.net

\end{document}
