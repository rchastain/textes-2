\documentclass[a4paper,12pt]{article}

\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage{microtype}
\usepackage{geometry}
\geometry{margin=24mm}

\usepackage{lettrine}

%~ \setmainfont[
    %~ Path           = libre-baskerville/,
    %~ Extension      = .otf,
    %~ Ligatures      = TeX,
    %~ BoldFont       = LibreBaskerville-Bold,
    %~ ItalicFont     = LibreBaskerville-Italic
%~ ]{LibreBaskerville-Regular}

%~ https://www.fontsquirrel.com/fonts/libre-baskerville

%~ \setmainfont{LibreBaskerville}

\setmainfont{EBGaramond}
[
  Extension      = .otf ,
  UprightFont    = *-Regular,
  ItalicFont     = *-Italic,
  BoldFont       = *-Bold,
  BoldItalicFont = *-BoldItalic,
  Numbers        = Lowercase,
  Ligatures      = Discretionary,
  Style          = Swash
]
% https://texnique.fr/osqa/questions/8423/lualatex-et-ligatures

\setlength{\columnsep}{6mm} 

%~ \usepackage{lastpage}
%~ \usepackage{fancyhdr}
%~ \pagestyle{fancy}
%~ \fancyhf{}
%~ \fancyfoot[C]{\thepage \hspace{1pt} / \pageref{LastPage}}
%~ \renewcommand{\headrulewidth}[0]{0pt}

\begin{document}

%~ \thispagestyle{empty}

\begingroup  
  \centering
  \LARGE Encore Jean-Jacques Rousseau\\[1em]
  \normalsize par\\[1em]
  \large Jacques B\textsc{ainville}\\[2.5em]\par
\endgroup

%~ \section*{\centering Adam, Ève, la famille.}

\lettrine{À}{ table}, l’autre soir, on parlait de Rousseau. C’est l’homme du jour et
il est de moins en moins probable que la fête de son bicentenaire soit
très heureuse pour sa mémoire.

Chaque fois qu’une affreuse curiosité ramène la pensée sur Jean-Jacques,
c’est pour découvrir chez lui un peu plus d’ignominie. Et pourtant, ce
livre monstrueux, ce musée des horreurs qui s’appelle les Confessions,
ce n’est pas un livre que le dégoût fasse refermer. Jean-Jacques a beau
être, selon le mot d’un personnage de M. Anatole France, un « plat
coquin », l’ouvrage où il s’est déshabillé et mortifié en public,
ouvrage écœurant, révoltant même si l’on veut, n’a pas la moindre
platitude. Le sortilège de l’art le soutient et plusieurs de ces
épisodes (celui du gué, celui des cerises, celui de la courtisane
vénitienne) resteront parmi les choses célèbres de la littérature de
tous les temps, en dépit de leur fausse innocence ou de leur troublante
impureté.

C’était un très grand, un très puissant écrivain que Jean-Jacques. Quel
critique a dit que sa période, pour l’ampleur, n’avait d’égale que celle
de Bossuet ? Il est certain que son action n’a été si profonde qu’en
raison du charme de sa voix. Je ne sais comment l’on peut soutenir
quelquefois que le peuple français n’est pas un peuple artiste, car il
faut que, vraies ou fausses, les idées lui soient présentées d’une
certaine manière pour entraîner son adhésion. Il y a un génie littéraire
à l’origine de toutes les révolutions de notre pays, et jamais l’ignoble
et barbare philosophie de Rousseau n’eût pu tourner les têtes françaises
si, par un surprenant hasard, ce Genevois n’eût joué d’un des
instruments les plus mélodieux dont ait disposé un homme. Il fait songer
à la légende de ce preneur de rats qui, en sonnant de sa délicieuse
musique, conduisit au fond de la rivière non seulement tous les rongeurs
qui infestaient la ville, mais les habitants de la ville avec eux.

La merveille, chez Rousseau, c’est que son infamie, son angoisse
nerveuse, son cynisme et, comme disaient de son temps ses adversaires,
sa « lycanthropie », ne soient pas exclusives d’une atmosphère poétique.
On y fut pris de son temps et la cour de Versailles elle-même voulut
entendre le Devin du village. Cette opérette parut fraîche. Le cœur de
Rousseau était cependant plus flétri et plus gros de turpitudes que
celui du plus roué des hommes du monde d’alors. Ça ne faisait rien : ce
rustre perverti répandait l’illusion de l’innocence. Il y a toujours eu
autour de lui un fluide ; je défie qu’on visite par exemple les
Charmettes sans se sentir baigné dans un air qui n’est pas celui
d’ailleurs. Ce n’est pas à dire que cet air soit ni pur ni sain.

Il s’en faut de beaucoup que l’histoire du laquais errant et de Mme de
Warens soit hygiénique et moins encore qu’elle soit belle. Il existe un
portrait de Mme de Warens par Largillière. Et Largillière, ce n’est pas
un peintre qui se tourmente, qui trouve des dessous à la nature humaine.
Son image de « maman » est néanmoins bien inquiétante et le dessin de la
bouche y dément étrangement la tranquillité du regard. On place, malgré
soi, Jean-Jacques d’un côté de ce tableau et Claude Anet de l’autre. On
se souvient de ce qui se passait dans la petite maison rustique de la
route de Chambéry et qui ne se raconterait pas décemment devant
d’honnêtes femmes. Mais quelle magie, sinon celle de l’écrivain, a pu
faire que les Charmettes soient tout de même restées comme un paysage
d’idylle ?

L’enthousiasme et la sensibilité firent même longtemps que cette espèce
de mauvais lieu en garda le caractère d’un temple. « Qui de nous,
écrivait un jour George Sand en parlant des Charmettes, n’y a pas vécu
en imagination les plus beaux jours de sa jeunesse ? » Merci bien,
madame. Mais « qui de nous », en 1912, paraît un peu exagéré, et nous
n’allons plus jusque-là.

Arsène Houssaye eut un jour la curiosité de dépouiller le registre où
les visiteurs des Charmettes consignent leurs impressions à la façon
immortelle de M. Perrichon. Il fit des trouvailles étonnantes pour toute
la période qui va de 1820 à 1860. Il y avait presque côte à côte
l’écriture d’une dame de la meilleure société qui se flattait d’avoir
cueilli dans le petit jardin « la dernière pervenche oubliée par
Rousseau », et la signature d’Orsini, « adepte et dévoué aux principes
de l’immortel Jean-Jacques ». La femme du monde et le terroriste, la
petite fleur et la bombe, c’est bien tout le génie de Rousseau, c’est
toute son influence et toute son œuvre, et c’est l’allégorie des ravages
qu’il a exercés parmi l’espèce humaine en général et sur la France en
particulier.

Un des logis qu’il habita est moins « poétique » que les Charmettes.
Allez le voir : c’est à Paris, en retrait de la rue Monge. Une plaque de
marbre le désigne aux passants. Là Rousseau vécut avec Thérèse, et c’est
de là, je pense, qu’il portait ses enfants au tour et ses livres
funestes à l’imprimeur. De pauvres gens, des prolétaires habitent de nos
jours ce taudis. Si l’ombre de Jean-Jacques vient les hanter,
doivent-ils faire de mauvais rêves !

\begin{flushright}
\large Jacques B\textsc{ainville}.\\
Paru dans \textit{L’Action française} le 23 juin 1912.
\end{flushright}

% www.biblisem.net

\end{document}
