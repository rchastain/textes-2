# Œuvres littéraires et philosophiques en langue française

Reproduction en LaTeX d'articles ou de livres en langue française.

Les textes, sauf ceux que j'ai *tapés* moi-même, proviennent des sites suivants :

- [Bibliotheca Augustana](https://www.hs-augsburg.de/~harsch/augustana.html)
- [Bibliothèque électronique du Québec](https://beq.ebooksgratuits.com)
- [Bibliothèque monastique Saint-Benoît](https://www.bibliotheque-monastique.ch)
- [BIBLISEM](http://www.biblisem.net)
- [Livres mystiques](https://livres-mystiques.com)
- [Project Gutenberg](https://gutenberg.org)
- [remacle.org](https://remacle.org)
- [Wikisource](https://fr.wikisource.org/wiki/Portail:Accueil)

## Table

- [BAINVILLE (Jacques), *Encore Jean-Jacques Rousseau.*](bainville/rousseau.pdf)
- [BARAUD (Armand), *De Bonald.*](baraud/de-bonald.pdf)
- [BARBEY D'AUREVILLY (Jules), *La Femme et l'Enfant.*](barbey-aurevilly/femme.pdf)
- [BARBEY D'AUREVILLY (Jules), *L'Internelle Consolacion.*](barbey-aurevilly/internelle-consolacion.pdf)
- [BARBIER (Paul), *Les Dieux de l’Irréligion : L’État-Dieu et le culte de la Révolution.*](barbier/etat-dieu.pdf)
- [BARBIER (Paul), *Les Dieux de l’Irréligion : La Raison et la Nature.*](barbier/raison.pdf)
- [BARBIER (Paul), *Les Propagateurs de l’Irréligion : Romans et romanciers.*](barbier/romans.pdf)
